import datetime

import ttn
import conf

from influxdb import InfluxDBClient


class UplinkHandler(object):
    def __init__(self):
        handler = ttn.HandlerClient(conf.app_id, conf.access_key)
        self._mqtt_client = handler.data()
        self._mqtt_client.set_uplink_callback(self.uplink_callback)
        self._influxdb_client = InfluxDBClient(
            conf.influxdb_server,
            conf.influxdb_port,
            conf.influxdb_user,
            conf.influxdb_password,
            conf.influxdb_database_name
        )

    def start(self):
        self._mqtt_client.connect()

    def close(self):
        self._mqtt_client.close()
        self._influxdb_client.close()

    def uplink_callback(self, msg, client):
        print("Received uplink from {}, DEVEUI: {}".format(msg.dev_id, msg.hardware_serial))
        print("Payload: {}".format(msg.payload_fields.text))
        print("Time: {}".format(msg.metadata.time))

        try:
            fields = self._parse_payload(msg.payload_fields.text)
            json_body = self._prepare_json(fields)
            self._influxdb_client.write_points(json_body)
        except Exception as e:
            print(f"Error during uplink callback: {e}")

    def _parse_payload(self, payload):
        null_index = payload.find('\0')
        if null_index != -1:
            payload = payload[0:null_index]
        print(f'Pure payload: {payload}')
        splitted = payload.split(',')
        fields = {}
        for measurement in splitted:
            type_of_meas, meas_value_str = measurement.split('=')
            if type_of_meas.upper() == 'T':
                fields["temperature"] = float(meas_value_str)
            elif type_of_meas.upper() == 'O':
                fields["light"] = float(meas_value_str)

        return fields

    def _prepare_json(self, fields):
        json_body = [
            {
                "measurement": "measurement",
                "tags": {
                    "location": "location",
                },
                "time": datetime.datetime.utcnow().isoformat(),
                "fields": fields
            }
        ]
        return json_body
