app_id = "app_id"
access_key = "access_key"
dev_id = "dev_id"

influxdb_server = 'server_addr'
influxdb_port = 8086
influxdb_user = 'user'
influxdb_password = 'password'
influxdb_database_name = 'database'
