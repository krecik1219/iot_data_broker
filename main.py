from downlink_handler import DownlinkHandler
from uplink_handler import UplinkHandler

uplink_handler = None
downlink_handler = None
should_terminate = False


def init_uplink_handling():
    print("init_uplink_handling")
    global uplink_handler
    uplink_handler = UplinkHandler()
    uplink_handler.start()


def init_downlink_handling():
    print("init_downlink_handling")
    global downlink_handler
    downlink_handler = DownlinkHandler()
    downlink_handler.start()


def handle_user_input():
    usr_input = str(input('Type msg to be sent to the device or "end" to stop this program: '))
    if usr_input is None or usr_input == '' or usr_input.isspace():
        return
    elif usr_input.lower() == 'end':
        global should_terminate
        should_terminate = True
    else:
        downlink_handler.send(usr_input)


if __name__ == '__main__':
    try:
        init_uplink_handling()
        init_downlink_handling()
        while not should_terminate:
            handle_user_input()
    except Exception as e:
        print(f'Error: {e}')
    finally:
        if uplink_handler is not None:
            uplink_handler.close()
        if downlink_handler is not None:
            downlink_handler.close()

    print("Terminated")
