# LoRa IoT MQTT
Venv and dependencies
```Shell
python -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
```

Provide your project credentials in conf.py, adjust json which will be sent to influxdb in uplink_handler.py
Run:
```Shell
python main.py
```
Script will forward data from TTN to you influxdb. You can type any input, it will be sent to your node device.
