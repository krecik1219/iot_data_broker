import base64

import ttn

import conf


class DownlinkHandler(object):
    def __init__(self):
        handler = ttn.HandlerClient(conf.app_id, conf.access_key)
        self._mqtt_client = handler.data()

    def start(self):
        self._mqtt_client.connect()

    def close(self):
        self._mqtt_client.close()

    def send(self, msg):
        payload = base64.b64encode(msg.encode()).decode()
        print(f'msg={msg}; payload={payload}')
        self._mqtt_client.send(dev_id=conf.dev_id, pay=payload, port=1, conf=False, sched="replace")
